package hu.elte;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Baby {


    private LocalDate birthDate;
    private String babyName;
    private char gender;

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthdate(LocalDate dob) {
        if (dob == null) {
            throw new IllegalArgumentException("The birth date must be a valid date, it cannot be empty");
        }
        this.birthDate = dob;

    }

    public void setBirthdate(String dobString) {
        if (dobString == null) {
            throw new IllegalArgumentException("The birth date must be a valid date, it cannot be empty");
        }
        this.birthDate = LocalDate.parse(dobString); //, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public Baby(String dobString, String babyName, char gender){

        this.birthDate = LocalDate.parse(dobString); //, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        this.babyName = babyName;
        validateGenderChar(gender);
        this.gender = gender;
    }

    public Baby(LocalDate dob, String babyName, char gender){

        this.birthDate = dob;
        this.babyName = babyName;
        validateGenderChar(gender);
        this.gender = gender;
    }

    private void validateGenderChar(char gender) {
        if (gender != 'f' && gender != 'm'){
            throw new IllegalArgumentException("The Gender of the baby is incorrect (use 'm' or 'f')");
        }
    }

    public boolean isOlder(Baby baby){
        return  this.getBirthDate().isBefore(baby.getBirthDate()) ;
    }

    public long howOld(){
        return ChronoUnit.DAYS.between(this.birthDate, LocalDate.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Baby baby = (Baby) o;
        if (gender != baby.gender) return false;
        if (!birthDate.equals(baby.birthDate)) return false;
        return babyName.equals(baby.babyName);
    }

    @Override
    public int hashCode() {
        int result = birthDate.hashCode();
        result = 31 * result + babyName.hashCode();
        result = 31 * result + (int) gender;
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "Name='" + babyName.toUpperCase() + '\'' +
                ", Age=" + this.howOld() +
                ", DOB=" + birthDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
                ", Gender=" + gender +
                '}';
    }

}
