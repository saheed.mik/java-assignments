package hu.elte;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class BabyRegister {

    private static boolean exit;
    private static String input = "";
    private static ArrayList<Baby> babies =  new ArrayList<>();
    private static ArrayList<Gift> gifts =  new ArrayList<>();

    public static void main(String[] args) {

        //at start:
        System.out.println("WELCOME TO THE BABY REGISTER APP");
        generateInitialData();
        printInstruction();

        while (!exit){
            input = createCursor();
            switch (input) {
                case "exit":
                    printExit();
                    exit = true;
                    break;
                case "help":
                    printInstruction();
                    break;
                case "add-baby":
                    System.out.println("Name of Baby : ");
                    String name = createCursor();
                    System.out.println("Baby's date of birth (dd-MM-yyyy) : ");
                    String dobString = createCursor();
                    LocalDate day = LocalDate.parse(dobString, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                    dobString =  day.toString();

                    System.out.println("Gender of baby (Type 'M' for male and 'F' for female) : ");
                    char gender = createCursor().toUpperCase().charAt(0);

                    babies.add(new Baby(dobString, name, gender));
                    System.out.println("SUCCESSFULL. Now you have " + babies.size() + "babies");
                    break;
                case "list-baby":
                    System.out.println("To Show the list sorted: by Age type 'age' ;OR by name type 'name' : ");
                    String choice = createCursor();
                    if (choice.equals("age")){
                        babies.sort(Comparator.comparingLong(x -> new Long(x.howOld())));
                        for (Baby baby : babies){
                            System.out.println(baby);
                        }
                    }else if(choice.equals("name")){
                        babies.sort(Comparator.comparing(x -> x.getBabyName()));
                        for (Baby baby : babies){
                            System.out.println(baby);
                        }
                    }else{
                        System.out.println("There seems to be an error in your input, please check and try again.");
                        printInstruction();
                    }
                    break;
                case "add-gift":
                    System.out.println("Name of the person giving the gift : ");
                    String giverName = createCursor();
                    System.out.println("Date the give was given (dd-MM-yyyy) : ");
                    String dateString = createCursor();
                    LocalDate dt = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                    dateString =  dt.toString();

                    System.out.println("Short Description of Gift : ");
                    String describe = createCursor();

                    gifts.add(new Gift(giverName, dateString, describe));
                    System.out.println("SUCCESSFULL. Now you have " + gifts.size() + "gifts");

                    break;
                case "list-gift":
                    gifts.sort(Comparator.comparing(x -> x.getGiversName()));
                    for (Gift gift : gifts){
                        System.out.println(gift);
                    }
                    break;
                default:
                    System.out.println("There seems to be an error in your input, please check and try again.");
                    printInstruction();
                    break;

            }
        }
    }

    private static void generateInitialData() {
        LocalDate day = LocalDate.parse("22-10-2015", DateTimeFormatter.ofPattern("dd-MM-yyyy"));//"22/10/2015"  "2015-10-22"
        //day = LocalDate(day,day.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        String name = "Sultan - ";
        char[] gender = {'m', 'f'};

        for (int i = 0; i <= 5; i++) {

            babies.add(new Baby(day.plusDays(i).toString(), name + i, gender[i%2]));
            babies.add(new Baby(day.plusDays(i), name + (20-i), gender[i%2]));
            gifts.add(new Gift("nice" + name + (i^2), day.plusDays(i + 7).toString(), "descrptn"));

        }
        System.out.println();
        System.out.println("INITIAL DATA HAS BEEN PRELOAD: " + babies.size() + " Babies and " + gifts.size() + " gifts.");
        System.out.println("BABIES : ");
        for ( Baby baby: babies  ) {
            System.out.println(baby);
        }
        System.out.println("GIFTS : ");
        for ( Gift gift: gifts  ) {
            System.out.println(gift);
        }
    }

    private static void printInstruction() {
        System.out.println();
        System.out.println();
        System.out.println("INSTRUCTIONS  : ");
        System.out.println("To exit the App : exit");
        System.out.println("To show instructions : help");
        System.out.println("To Add new Baby : add-baby");
        System.out.println("To List babies : list-baby");
        System.out.println("To Add new gift : add-gift");
        System.out.println("To List gifts: list-gift");
        System.out.println();
    }

    private static String createCursor(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("BabyReg>");
        String input;
        input = scanner.next();
        return input;
    }

    private static void printExit(){
        System.out.println("Goodbye");
    }
}
