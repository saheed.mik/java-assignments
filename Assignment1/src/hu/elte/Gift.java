package hu.elte;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Gift {
    private String giversName;
    private LocalDate dateGiven;
    private String description;

    public String getGiversName() {
        return giversName;
    }

    public void setGiversName(String giversName) {
        this.giversName = giversName;
    }

    public LocalDate getDateGiven() {
        return dateGiven;
    }

    public void setDateGiven(LocalDate dateGiven) {
        this.dateGiven = dateGiven;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Gift(String giversName, String dateGiven, String description) {
        this.giversName = giversName;
        this.dateGiven = LocalDate.parse(dateGiven); //, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        this.description = description;
    }

    @Override
    public String toString() {
        return "{" +
                "Giver's Name='" + giversName.toUpperCase() + '\'' +
                ", Date Given=" + dateGiven.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
                ", Description='" + description + '\'' +
                '}';
    }
}
