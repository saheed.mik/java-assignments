package elte.Assignment2;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MainTest {

//ToDo 1    Register guest (where you can register all the guest information)
//ToDo 2    Remove a guest (the system asks the name and deletes the user)
//ToDo 3    Pick a song (after selection the song goes automatically for the list of played songs)
//ToDo 4    List played songs
//ToDo 5    Check if an animal is on the Guest list
//ToDo 6    List animal needs by animal name
//ToDo 7    List animal needs by specie name

//    Mufasa may:
//      Add a new guest (Name, specie name, song)
//      Remove a guest
//    Rafiki may:
//      Check if a guest is allowed by its name
//      Check which are the needs by animal name (e.g. Simba) or specie name (e.g. Lion)
//    Zazu may:
//      Pick a random song
//      Check which songs were already played
//
//    Flying: The extension of its wings
//    Swimming: Where the animal will come from (sea or river)
//    Main entrance: Number of paws

    FlyingAnimal flyer;
    SwimmingAnimal swimer;
    CrawlingAnimal crawler;
    Main mainObject;

    @Before
    public void setUp() throws Exception {
        mainObject = new Main();
        flyer = (FlyingAnimal) Specie.createNewSpecie("Eagle", Specie.FLY, "40 cm");
        swimer = (SwimmingAnimal) Specie.createNewSpecie("Shark", Specie.SWIM, "Ocean");
        crawler = (CrawlingAnimal) Specie.createNewSpecie("Lion", Specie.CRAWL, "4");
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSpecieNameUnavailable(){
        Animal guest2 = mainObject
                .addGuest("Simba", "Tiger", "America! First! by Donald Trump");
        assertEquals(1, mainObject.getGuests().size());
    }

    @Test
    public void testCreatUniqueGuests(){
        Animal guest1 = mainObject
                .addGuest("Simba", "Lion", "Stable Genius by Donald Trump");
        Animal guest2 = mainObject
                .addGuest("Simba", "Shark", "America! First! by Donald Trump");

        assertEquals(2, mainObject.getSongs().size());
        assertEquals(1, mainObject.getGuests().size());
    }

    @Test
    public void testCaseSensitive(){
        Animal guest1 = mainObject
                .addGuest("Simba", "Lion", "Stable Genius by Donald Trump".toLowerCase());
        Animal guest2 = mainObject
                .addGuest("Simba".toLowerCase(), "Shark", "Stable Genius by Donald Trump");

        assertEquals(1, mainObject.getSongs().size());
        assertEquals(1, mainObject.getGuests().size());
    }

    @Test
    public void testPickSongs(){
        Animal guest1 = mainObject
                .addGuest("Simba", "Lion", "Stable Genius by Donald Trump");
        Animal guest2 = mainObject
                .addGuest("Simba", "Shark", "America! First! by Donald Trump");
        try {
            assertEquals(2, mainObject.getSongs().stream().filter(i -> !i.isPlayed()).count());
            Main.pickSong();
            assertEquals(1, mainObject.getSongs().stream().filter(i -> !i.isPlayed()).count());
            Main.pickSong();
            assertEquals(0, mainObject.getSongs().stream().filter(i -> !i.isPlayed()).count());
            Main.pickSong();
        } catch (Exception e) {
            assertEquals("All Songs on the list have been played.", e.getMessage());
        }
    }

    @Ignore ("Sorry sir, I am late")
    public void main() {
        Main.main(null);
    }


}