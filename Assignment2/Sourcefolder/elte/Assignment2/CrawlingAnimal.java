package elte.Assignment2;

public class CrawlingAnimal extends Specie {

    private String specieNeeds;

    public CrawlingAnimal(String specieName, String numberOfPaws) {
        super(specieName);
        this.specieNeeds = "Has " + numberOfPaws + " paws.";
    }


    @Override
    String getSpecieNeeds() {
        return specieNeeds;
    }

    @Override
    public String toString() {
        return getSpecieName() + " :- " + getSpecieNeeds();
    }
}
