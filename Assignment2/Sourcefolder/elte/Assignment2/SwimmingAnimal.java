package elte.Assignment2;

public class SwimmingAnimal extends Specie{
    private String specieNeeds;

    public SwimmingAnimal(String specieName, String modeOfArrival) {
        super(specieName);
        this.specieNeeds = "Arrive through " + modeOfArrival + ".";
    }

    @Override
    String getSpecieNeeds() {
        return specieNeeds;
    }

    @Override
    public String toString() {
        return getSpecieName() + " :- " + getSpecieNeeds();
    }
}
