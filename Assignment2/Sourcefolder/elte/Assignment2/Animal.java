package elte.Assignment2;

import java.util.Objects;

public class Animal {
    private String name;
    private String specie;

    public Animal(String name, String specie) throws IllegalArgumentException {
        if (!Specie.isSpecieExist(specie)){
            throw new IllegalArgumentException("Specie name does not exist");
        }else {
            this.name = name;
            this.specie = specie;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Specie getSpecie() {
        return Specie.getSpecieByName(specie);
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(getName(), animal.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }
}
