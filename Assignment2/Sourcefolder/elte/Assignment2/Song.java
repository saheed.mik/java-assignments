package elte.Assignment2;

import java.util.*;

public class Song {
    private String name;
    private boolean isPlayed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPlayed() {
        return isPlayed;
    }

    public void setPlayed(boolean played) {
        isPlayed = played;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return Objects.equals(getName(), song.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    public Song(String songName) {
        name = songName;
    }

    public Song(String songName, boolean isPlayed){
        this(songName);
        this.isPlayed = isPlayed;
    }
}