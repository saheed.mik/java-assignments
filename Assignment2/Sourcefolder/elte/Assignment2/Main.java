package elte.Assignment2;


import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static Set<Animal> guests;
    private static Set<Song> songs;
    private static boolean exit;
    private static String input = "";

    private static FlyingAnimal flyer;
    private static SwimmingAnimal swimer;
    private static CrawlingAnimal crawler;

    public Main(){
    }

    public static void main(String[] args){
        guests = new HashSet<>();
        songs = new HashSet<>();
        createPrerequisiteObjects();
        System.out.println("WELCOME TO THE ANIMAL Jungle Party REGISTER APP");
        printInstruction();

        while (!exit){
            input = createCursor().toLowerCase();
            switch (input) {
                case "exit":
                    printExit();
                    exit = true;
                    break;
                case "help":
                    printInstruction();
                    break;
                case "r":
                    System.out.println("Guest Registration : ".toUpperCase() + "\n");
                    System.out.println(
                            "In this Kingdom there are only 3 species of animals :" + "\n" +
                                    flyer.getSpecieName().toUpperCase() + ", " +
                                    swimer.getSpecieName().toUpperCase() + ", " +
                                    crawler.getSpecieName().toUpperCase() + "\n" +
                                    flyer + "\n" + swimer + "\n" + crawler + "\n" + "\n"
                    );
                    System.out.println("What is the name of the animal : ");
                    String animalName = createCursor().toLowerCase();
                    System.out.println("What is the name of its specie : ");
                    String specieName = createCursor().toLowerCase();
                    System.out.println("What song would " + animalName.toUpperCase() + " like to hear  : ");
                    String songName = createCursor().toLowerCase();
                    addGuest(animalName, specieName, songName);
                    printGuestCount();
                    break;
                case "d":
                    System.out.println("Guest De-registration (DELETE) : " + "\n");
                    System.out.println("What is the name of the animal to remove from guest list: ");
                    String animalToRemoveName = createCursor().toLowerCase();
                    guests.removeIf(i -> i.getName().equals(animalToRemoveName));
                    printGuestCount();
                    break;
                case "p":
                    System.out.println("You have chosen to pick a song to play (RANDOM SELECTION). " + "\n");
                    try {
                        pickSong();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    System.out.println("There are now " + songs.stream().filter(i -> !i.isPlayed()).count() + "songs to be played.");
                    break;
                case "l":
                    System.out.println("You have chosen to show the list of played songs : " + "\n");
                    songs.stream().filter(i -> i.isPlayed())
                            .forEach(song -> System.out.println(song.getName()));
                    break;
                case "n":
                    System.out.println("You have chosen to show the list of animal needs by Animals' name : " + "\n");
                    guests.stream()
                            .sorted((i,j) -> i.getName().compareToIgnoreCase(j.getName()))
                            .forEach(g ->
                                    System.out.println(g.getName() + " => " + g.getSpecie().getSpecieNeeds())
                            );
                    break;
                case "s":
                    System.out.println("You have chosen to show the list of animal needs by Animals' specie : " + "\n");
                    guests.stream()
                            .sorted((i,j) -> i.getSpecie().getSpecieName().compareToIgnoreCase(j.getSpecie().getSpecieName()))
                            .forEach(g ->
                                    System.out.println(g.getSpecie().getSpecieName() + " => " + g.getSpecie().getSpecieNeeds())
                            );
                    break;
                default:
                    System.out.println("There seems to be an error in your input, please check and try again.");
                    printInstruction();
                    break;

            }
        }
    }

    private static void printGuestCount() {
        System.out.println("Now there are " + guests.size() + " guests.");
    }

    private static void createPrerequisiteObjects() {
        flyer = (FlyingAnimal) Specie.createNewSpecie("Eagle", Specie.FLY, "40 cm");
        swimer = (SwimmingAnimal) Specie.createNewSpecie("Shark", Specie.SWIM, "Ocean");
        crawler = (CrawlingAnimal) Specie.createNewSpecie("Lion", Specie.CRAWL, "4");

    }

    private static void printInstruction() {

        System.out.println();
//        System.out.println();
        System.out.println("INSTRUCTIONS  : ");
        System.out.println("To exit the App: \"exit\"");
        System.out.println("To show instructions: \"help\"");
        System.out.println("To Register guest: \"r\"");
        System.out.println("To Remove a guest: \"d\"");
        System.out.println("To Pick a song: \"p\"");
        System.out.println("To List played songs: \"l\"");
        System.out.println("To Check if an animal is on the Guest list: \"c\"");
        System.out.println("To List animal needs by animal name: \"n\"");
        System.out.println("To List animal needs by specie name: \"s\"");

        System.out.println();
    }

    private static String createCursor(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("PartyReg>");
        String input;
        input = scanner.next();
        return input;
    }

    private static void printExit(){
        System.out.println("Goodbye");
    }
    public static Animal addGuest(String animalName, String specieName, String songName){
        Animal animal = null;
        try {
            animal= new Animal(animalName.toLowerCase(), specieName.toLowerCase());
            guests.add(animal);
            songs.add(new Song(songName.toLowerCase()));
        } catch (IllegalArgumentException e) {
            System.out.println("\n" + "ERROR :- " + e.getMessage());
            printInstruction();
            //TODO: Handle Properly => Species not created Yet or wrong one chosen
        }
        return animal;
    }

    public static void pickSong() throws Exception {
        List<Song> sng = songs.stream().filter(i -> !i.isPlayed()).collect( Collectors.toList() );
        if (!sng.isEmpty()){
            Random random = new Random();
            int rand = random.nextInt(sng.size());
            Song song = sng.get(rand);
            song.setPlayed(true);
            System.out.println("The song \"" + song.getName() + "\" is now playing.");
        }else {

                throw new Exception("All Songs on the list have been played.");
        }

    }

    public Set<Animal> getGuests() {
        return guests;
    }

    public Set<Song> getSongs() {
        return songs;
    }
}
