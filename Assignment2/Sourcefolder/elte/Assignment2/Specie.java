package elte.Assignment2;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

public abstract class Specie {
    public static final int FLY = 1;
    public static final int SWIM = 2;
    public static final int CRAWL = 3;
    private static ArrayList<Specie> species = new ArrayList<>();

    private String specieName;

    protected Specie(String specieName) {
        this.specieName = specieName;
    }

    abstract String getSpecieNeeds();

    public String getSpecieName() {
        return specieName.toLowerCase();
    }

    public static Specie createNewSpecie(String specieName, int specieType, String specieNeeds ) {
//        Optional<Specie> specie = species.stream().findAny(s ->s.getSpecieName().equals(specieName));

        for (Specie sp : species) {
            if (sp.getSpecieName().equalsIgnoreCase(specieName)){
                return sp;
            }
        }

        switch (specieType){
            case 1:
                FlyingAnimal fly = new FlyingAnimal(specieName, specieNeeds);
                species.add(fly);
                return fly;
            case 2:
                SwimmingAnimal swim = new SwimmingAnimal(specieName, specieNeeds);
                species.add(swim);
                return swim;
            case 3:
                CrawlingAnimal craw = new CrawlingAnimal(specieName, specieNeeds);
                species.add(craw);
                return craw;
            default: throw new IllegalArgumentException("The supplied specie type is not available");

        }

    }

    public static boolean isSpecieExist(String specieName){
        return species.stream().anyMatch(i -> i.getSpecieName().equalsIgnoreCase(specieName));
    }

    public static ArrayList<Specie> getAllSpecies(){
        return species;
    }

    public static Specie getSpecieByName(String specieName) {
        Optional<Specie> specie = species.stream().filter(i -> i.getSpecieName().equalsIgnoreCase(specieName)).findFirst();
        return specie.isPresent()? specie.get() : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Specie that = (Specie) o;
        return Objects.equals(getSpecieName(), that.getSpecieName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getSpecieName());
    }

}
