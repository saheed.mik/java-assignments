package elte.Assignment2;

public class FlyingAnimal extends Specie{
    private String specieNeeds;

    public FlyingAnimal(String specieName, String lengthOfWings) {
        super(specieName);
        this.specieNeeds = "Length of wing is " + lengthOfWings + ".";
    }

    @Override
    String getSpecieNeeds() {
        return specieNeeds;
    }

    @Override
    public String toString() {
        return getSpecieName() + " :- " + getSpecieNeeds();
    }
}
